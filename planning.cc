/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/planning/planning.h"

#include <algorithm>
#include <list>
#include <vector>

#include "google/protobuf/repeated_field.h"

#include "modules/common/adapters/adapter_manager.h"
#include "modules/common/time/time.h"
#include "modules/common/vehicle_state/vehicle_state_provider.h"
#include "modules/map/hdmap/hdmap_util.h"
#include "modules/planning/common/planning_gflags.h"
#include "modules/planning/common/planning_thread_pool.h"
#include "modules/planning/common/planning_util.h"
#include "modules/planning/common/trajectory/trajectory_stitcher.h"
#include "modules/planning/planner/em/em_planner.h"
#include "modules/planning/planner/lattice/lattice_planner.h"
#include "modules/planning/planner/navi/navi_planner.h"
#include "modules/planning/planner/rtk/rtk_replay_planner.h"
#include "modules/planning/reference_line/reference_line_provider.h"
#include "modules/planning/tasks/traffic_decider/traffic_decider.h"

namespace apollo {
namespace planning {

using apollo::common::ErrorCode;
using apollo::common::Status;
using apollo::common::TrajectoryPoint;
using apollo::common::VehicleState;
using apollo::common::VehicleStateProvider;
using apollo::common::adapter::AdapterManager;
using apollo::common::time::Clock;
using apollo::common::PointENU;
using apollo::hdmap::HDMapUtil;
using apollo::hdmap::LaneInfo;

Planning::~Planning() { Stop(); }

std::string Planning::Name() const { return "planning"; }

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// This function is used to transfer the id of lane from char to  double. 
// Format: 1954ac_45_21-1   --->>>   '195445211'+'ac'  

int cp = 0; 

double char2double(const char* id){
    std::string result(id);
    std::string::iterator it;
    std::string final = "";
    int tmp = 0;
    for (it = result.begin(); it != result.end(); it++){
        if(*it == '-' || *it == '_');
        else if((int)*it <= 57 && (int)*it >= 48 ){
            final += *it;
        }
        else {
            tmp += (int)*it; 
        }
    }
    if (tmp != 0){
        final += std::to_string(tmp);
    }

    while (final.size() <= 9){
      final += '0';
    }
    if (final.size() > 10){
      final = final.substr(final.size()-10,10);
    }
    double final_double = std::stod(final);
    return final_double;
}

bool is_in_vector(double test_item, std::vector<double> list){
  bool flags = false;
  for(std::vector<double>::iterator iter = list.begin(); iter != list.end(); iter++){
    if (test_item == *iter){
      flags = true;
    }
  }
  return flags;
}

// This function is used to parse the solution of commonroad, finally we will get a publishable
// ADC trajectory.
ADCTrajectory parse_trajectory(PyObject *result, const apollo::common::PointENU ego_point){

  ADCTrajectory trajectory_cr;

  // parse solution of trajectory for ks Model.
  if (cp == 0) { //Jiaying
    cp = PyList_Size(result)*10-9;
  }
  for (int i = 0; i < cp; i++){ //Jiaying
    if (i == 0){
      PyObject* state = PyList_GetItem(result, i);
      auto* state_point = trajectory_cr.add_trajectory_point();

      PyObject* x = PyDict_GetItem(state, PyUnicode_FromString("x"));
      state_point->mutable_path_point()->set_x(PyFloat_AsDouble(x) + ego_point.x());

      PyObject* y = PyDict_GetItem(state, PyUnicode_FromString("y"));
      state_point->mutable_path_point()->set_y(PyFloat_AsDouble(y) + ego_point.y());

      PyObject* o = PyDict_GetItem(state, PyUnicode_FromString("o"));
      state_point->mutable_path_point()->set_theta(PyFloat_AsDouble(o));

      PyObject* v = PyDict_GetItem(state, PyUnicode_FromString("v"));
      state_point->set_v(PyFloat_AsDouble(v));

      PyObject* a = PyDict_GetItem(state, PyUnicode_FromString("a"));
      state_point->set_a(PyFloat_AsDouble(a));

      PyObject* relative_time = PyDict_GetItem(state, PyUnicode_FromString("relative_time"));
      state_point->set_relative_time(PyFloat_AsDouble(relative_time));
    }
    else{
      double ratio = ((i-1)%10+1)/10.0;
      PyObject* state_pre = PyList_GetItem(result, (i-1)/10);
      PyObject* state_suc = PyList_GetItem(result, (i-1)/10+1);
      auto* state_point = trajectory_cr.add_trajectory_point();

      auto x = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("x")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("x")))*ratio;
      state_point->mutable_path_point()->set_x(x + ego_point.x());

      auto y = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("y")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("y")))*ratio;
      state_point->mutable_path_point()->set_y(y + ego_point.y());
      
      auto diff = PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("o")))-PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("o")));//Jiaying_ratio
      if (diff>3.14159){
        diff = diff - 2*3.14159;
      }
      else if (diff<-3.14159){
        diff = diff + 2*3.14159;
      }
      diff = diff - (int)(diff/(2*3.1415926))*2*3.1415926;
      auto o = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("o"))) + diff*ratio;
      state_point->mutable_path_point()->set_theta(o);

      auto v = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("v")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("v")))*ratio;
      state_point->set_v(v);

      auto a = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("a")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("a")))*ratio;
      state_point->set_a(a);

      auto relative_time = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("relative_time")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("relative_time")))*ratio;
      state_point->set_relative_time(relative_time);
    }
  }

  for (int i = cp-1+5; i < PyList_Size(result)*10-9; i=i+5){ //Jiaying
    double ratio = ((i-1)%10+1)/10.0;
    PyObject* state_pre = PyList_GetItem(result, (i-1)/10);
    PyObject* state_suc = PyList_GetItem(result, (i-1)/10+1);
    auto* state_point = trajectory_cr.add_trajectory_point();

    auto x = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("x")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("x")))*ratio;
    state_point->mutable_path_point()->set_x(x + ego_point.x());

    auto y = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("y")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("y")))*ratio;
    state_point->mutable_path_point()->set_y(y + ego_point.y());

    //auto o = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("o")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("o")))*ratio;
    auto o = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("o")));
    state_point->mutable_path_point()->set_theta(o);

    auto v = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("v")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("v")))*ratio;
    state_point->set_v(v);

    auto a = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("a")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("a")))*ratio;
    state_point->set_a(a);

    auto relative_time = PyFloat_AsDouble(PyDict_GetItem(state_pre, PyUnicode_FromString("relative_time")))*(1-ratio) + PyFloat_AsDouble(PyDict_GetItem(state_suc, PyUnicode_FromString("relative_time")))*ratio;
    state_point->set_relative_time(relative_time);
  }

  return trajectory_cr;

}


// Parses obstacle information of Apollo environment into a pyobject dict.//
PyObject* obstacle_parser(
  const std::vector<double> id_vec,
  const std::vector<double> pos_x_vec, 
  const std::vector<double> pos_y_vec,
  const std::vector<double> vel_x_vec, 
  const std::vector<double> vel_y_vec,
  const std::vector<double> theta_vec,
  const std::vector<double> length_vec,
  const std::vector<double> width_vec,
  const std::vector<double> type_vec){

  PyObject* obstacleDict = PyDict_New(); // stores all the info to be sent Python script

  PyObject* key_pos_x = PyUnicode_FromString("pos_x");
  PyObject* key_pos_y = PyUnicode_FromString("pos_y");
  PyObject* key_v = PyUnicode_FromString("v");
  PyObject* key_theta = PyUnicode_FromString("theta");
  PyObject* key_length = PyUnicode_FromString("length");
  PyObject* key_width = PyUnicode_FromString("width");
  PyObject* key_type = PyUnicode_FromString("type");

  for (unsigned int i = 0; i < id_vec.size(); i++){

    PyObject* datum = PyDict_New();

    // obstacle id
    PyObject* key_id = PyUnicode_FromString(std::to_string(id_vec[i]).c_str());

    // position info    
    PyObject* val_pos_x = PyFloat_FromDouble(pos_x_vec[i]);
    PyObject* val_pos_y = PyFloat_FromDouble(pos_y_vec[i]);

    // velocity info    
    PyObject* val_v = PyFloat_FromDouble(sqrt(pow(vel_x_vec[i],2) + pow(vel_y_vec[i],2)));

    // orientation info    
    PyObject* val_theta = PyFloat_FromDouble(theta_vec[i]);

    // length and width info    
    PyObject* val_length = PyFloat_FromDouble(length_vec[i]);
    PyObject* val_width = PyFloat_FromDouble(width_vec[i]);

    // obtacle type info    
    PyObject* val_type = PyFloat_FromDouble(type_vec[i]);

    // put regarding info about current obstacle into a dictionary
    // obstacle's perception results
    PyDict_SetItem(datum, key_pos_x, val_pos_x);
    PyDict_SetItem(datum, key_pos_y, val_pos_y);
    PyDict_SetItem(datum, key_v, val_v);
    PyDict_SetItem(datum, key_theta, val_theta);
    PyDict_SetItem(datum, key_length, val_length);
    PyDict_SetItem(datum, key_width, val_width);
    PyDict_SetItem(datum, key_type, val_type);
    
    // set obstacle data in message dict of which key value is its ID 
    PyDict_SetItem(obstacleDict, key_id, datum);
  }

  return obstacleDict;
}

// Parse lane information into a pyobject dict.
PyObject* lane_parser(std::vector<double> vector_id,
  std::vector<std::vector<double>> vector_left_boundary_x,
  std::vector<std::vector<double>> vector_left_boundary_y,
  std::vector<std::vector<double>> vector_right_boundary_x,
  std::vector<std::vector<double>> vector_right_boundary_y,  
  std::vector<std::vector<double>> vector_succ_ids, 
  std::vector<std::vector<double>> vector_pred_ids, 
  std::vector<std::vector<double>> vector_lf_ids, 
  std::vector<std::vector<double>> vector_lr_ids,
  std::vector<std::vector<double>> vector_rf_ids,
  std::vector<std::vector<double>> vector_rr_ids,
  std::vector<double> vector_speed_limit){
  //auto state = PyGILState_Ensure();
  PyObject *laneDict = PyDict_New();

  for (unsigned int n = 0; n < vector_id.size(); n++){
    // set all keys term and value term for dict.
    PyObject *key_id = PyLong_FromDouble(vector_id[n]);
    PyObject *data = PyDict_New();

    PyObject *key_left_boundary = PyUnicode_FromString("left_boundary");
    PyObject *val_left_boundary = PyList_New(vector_left_boundary_x[n].size());

    PyObject *key_central_boundary = PyUnicode_FromString("central_boundary");
    PyObject *val_central_boundary = PyList_New(vector_left_boundary_y[n].size());

    PyObject *key_right_boundary = PyUnicode_FromString("right_boundary");
    PyObject *val_right_boundary = PyList_New(vector_right_boundary_x[n].size());
     
    PyObject *key_succ_ids = PyUnicode_FromString("succ_ids");
    PyObject *val_succ_ids = PyList_New(0);

    PyObject *key_pred_ids = PyUnicode_FromString("pred_ids");
    PyObject *val_pred_ids = PyList_New(0);


    PyObject *key_l_ids = PyUnicode_FromString("l_ids");
    PyObject *key_r_ids = PyUnicode_FromString("r_ids");
    PyObject *val_l_ids = PyTuple_New(2);
    PyObject *val_r_ids = PyTuple_New(2);

    PyObject *val_lf_ids = PyList_New(0);
    PyObject *val_lr_ids = PyList_New(0);
    PyObject *val_rf_ids = PyList_New(0);
    PyObject *val_rr_ids = PyList_New(0);  
    
    PyObject *key_speed_limit = PyUnicode_FromString("speed_limit");
    PyObject *val_speed_limit = PyFloat_FromDouble(vector_speed_limit[n]);

    //put all infomation required into the value term
    for (unsigned int j = 0; j < vector_left_boundary_x[n].size(); j++){
      PyList_SetItem(val_left_boundary, j, Py_BuildValue("dd",vector_left_boundary_x[n][j], vector_left_boundary_y[n][j]));
    }
    
    for (unsigned int j = 0; j < vector_left_boundary_y[n].size(); j++){
      PyList_SetItem(val_central_boundary, j, Py_BuildValue("dd",(vector_left_boundary_x[n][j] + vector_right_boundary_x[n][j])/2, (vector_left_boundary_y[n][j] + vector_right_boundary_y[n][j])/2));
    }

    for (unsigned int j = 0; j < vector_right_boundary_x[n].size(); j++){
      PyList_SetItem(val_right_boundary, j, Py_BuildValue("dd",vector_right_boundary_x[n][j], vector_right_boundary_y[n][j]));
    }

    for (unsigned int j = 0; j < vector_succ_ids[n].size(); j++){
      if (is_in_vector(vector_succ_ids[n][j], vector_id)){
        PyList_Append(val_succ_ids, PyLong_FromDouble(vector_succ_ids[n][j]));
      }
    }

    for (unsigned int j = 0; j < vector_pred_ids[n].size(); j++){
      if (is_in_vector(vector_pred_ids[n][j], vector_id)){
        PyList_Append(val_pred_ids, PyLong_FromDouble(vector_pred_ids[n][j]));
      }
    }
  
    for (unsigned int j = 0; j < vector_lf_ids[n].size(); j++){
      if (is_in_vector(vector_lf_ids[n][j], vector_id)){
        PyList_Append(val_lf_ids, PyLong_FromDouble(vector_lf_ids[n][j]));
      }
    }
     
    for (unsigned int j = 0; j < vector_lr_ids[n].size(); j++){
      if (is_in_vector(vector_lr_ids[n][j], vector_id)){
        PyList_Append(val_lr_ids, PyLong_FromDouble(vector_lr_ids[n][j]));
      }
    }

    if (PyList_Size(val_lf_ids) != 0 && PyList_Size(val_lr_ids) == 0){
        PyTuple_SetItem(val_l_ids, 0, PyList_GET_ITEM(val_lf_ids, 0));
        PyTuple_SetItem(val_l_ids, 1, Py_True);  
    }
    else if (PyList_Size(val_lf_ids) == 0 && PyList_Size(val_lr_ids) != 0){
        PyTuple_SetItem(val_l_ids, 0, PyList_GET_ITEM(val_lr_ids, 0));
        PyTuple_SetItem(val_l_ids, 1, Py_False);
    }
    else {
        PyTuple_SetItem(val_l_ids, 0, Py_None);
        PyTuple_SetItem(val_l_ids, 1, Py_None);
    }

    for (unsigned int j = 0; j < vector_rf_ids[n].size(); j++){
      if (is_in_vector(vector_rf_ids[n][j], vector_id)){
        PyList_Append(val_rf_ids, PyLong_FromDouble(vector_rf_ids[n][j]));
      }
    }
  
    for (unsigned int j = 0; j < vector_rr_ids[n].size(); j++){
      if (is_in_vector(vector_rr_ids[n][j], vector_id)){
        PyList_Append(val_rr_ids, PyLong_FromDouble(vector_rr_ids[n][j]));
      }
    }

    if (PyList_Size(val_rf_ids) != 0 && PyList_Size(val_rr_ids) == 0){
        PyTuple_SetItem(val_r_ids, 0, PyList_GET_ITEM(val_rf_ids, 0));
        PyTuple_SetItem(val_r_ids, 1, Py_True);  
    }
    else if (PyList_Size(val_rf_ids) == 0 && PyList_Size(val_rr_ids) != 0){
        PyTuple_SetItem(val_r_ids, 0, PyList_GET_ITEM(val_rr_ids, 0));
        PyTuple_SetItem(val_r_ids, 1, Py_False);
    }
    else {
        PyTuple_SetItem(val_r_ids, 0, Py_None);
        PyTuple_SetItem(val_r_ids, 1, Py_None);
    }   

    PyDict_SetItem(data, key_left_boundary, val_left_boundary);
    PyDict_SetItem(data, key_central_boundary, val_central_boundary);
    PyDict_SetItem(data, key_right_boundary, val_right_boundary);
    PyDict_SetItem(data, key_succ_ids, val_succ_ids);
    PyDict_SetItem(data, key_pred_ids, val_pred_ids);
    PyDict_SetItem(data, key_l_ids, val_l_ids);
    PyDict_SetItem(data, key_r_ids, val_r_ids);
    PyDict_SetItem(data, key_speed_limit, val_speed_limit);
    PyDict_SetItem(laneDict, key_id, data);

  }

  return laneDict;
}

// This function is used to extract obstacles from the frame.
PyObject* extract_obstacle(
  const std::vector<const Obstacle*>& obstacles,
  const apollo::common::PointENU ego_point){

  // Obstacle's perception info
  std::vector<double> id_vec;

  std::vector<double> pos_x_vec;
  std::vector<double> pos_y_vec;

  std::vector<double> vel_x_vec;
  std::vector<double> vel_y_vec;

  std::vector<double> theta_vec;
  std::vector<double> length_vec;
  std::vector<double> width_vec;
  std::vector<double> type_vec;

  // get obstacle info from frame_ object
  for(const auto& iter: obstacles){

    // Perception info  
    const auto percep = iter->Perception();

    double id = (double)percep.id(); // id
    
    double pos_x = percep.position().x() - ego_point.x(); // x-comp of position
    double pos_y = percep.position().y() - ego_point.y(); // y-comp of position
    
    double vel_x = percep.velocity().x(); // x-comp of velocity
    double vel_y = percep.velocity().y(); // y-comp of velocity

    double theta = percep.theta(); // theta (orientation/heading)
    double length = percep.length(); // length
    double width = percep.width();
    double t = -1; // type of the PerceptionObstacle

    switch(percep.type()){
       case apollo::perception::PerceptionObstacle_Type_UNKNOWN_MOVABLE: t = 1; break;
       case apollo::perception::PerceptionObstacle_Type_UNKNOWN_UNMOVABLE: t = 2; break;
       case apollo::perception::PerceptionObstacle_Type_PEDESTRIAN: t = 3; break;
       case apollo::perception::PerceptionObstacle_Type_BICYCLE: t = 4; break;
       case apollo::perception::PerceptionObstacle_Type_VEHICLE: t = 5; break;
       default: t = 0; break;
    }

    id_vec.push_back(id);
    pos_x_vec.push_back(pos_x);
    pos_y_vec.push_back(pos_y);
    vel_x_vec.push_back(vel_x);
    vel_y_vec.push_back(vel_y);
    theta_vec.push_back(theta);
    length_vec.push_back(length);
    width_vec.push_back(width);
    type_vec.push_back(t);
  }

  PyObject* obstacle_dict = obstacle_parser(id_vec, 
                                          pos_x_vec, 
                                          pos_y_vec, 
                                          vel_x_vec, 
                                          vel_y_vec, 
                                          theta_vec,
                                          length_vec,
                                          width_vec, 
                                          type_vec); 
  return obstacle_dict;
}

// This function is used to extract lane from the apollo.//
PyObject* extract_lane(const apollo::common::PointENU ego_point,
  const hdmap::HDMap* hdmap_cr){

  std::vector<std::shared_ptr<const LaneInfo>> surrounding_lanes_for_commonroad;

  std::vector<double> lane_ids_;
  std::vector<double> succ_ids_, pred_ids_;
  std::vector<double> lf_ids_, lr_ids_, rf_ids_, rr_ids_;

  std::vector<std::vector<double>> succ_ids_all, pred_ids_all;
  std::vector<std::vector<double>> lf_ids_all, lr_ids_all, rf_ids_all, rr_ids_all;

  std::vector<double> speed_li_;

  std::vector<double> vector_x_l, vector_y_l;
  std::vector<std::vector<double>> vector_x_l_all, vector_y_l_all;
  std::vector<double> vector_x_r, vector_y_r;
  std::vector<std::vector<double>> vector_x_r_all, vector_y_r_all;

  hdmap_cr->GetLanes(ego_point, 100.0, &surrounding_lanes_for_commonroad);

  for (const auto& each_lane : surrounding_lanes_for_commonroad) {

    ///get ID of lanes    
    lane_ids_.push_back(char2double(each_lane->id().id().data()));
    
    ///get left_boundary
    auto lines_for_cr_l = each_lane->lane().left_boundary().curve().segment(0).line_segment(); 
    
    ///get all points of left_boundary
    std::vector<PointENU> points_L(lines_for_cr_l.point().begin(),lines_for_cr_l.point().end());
    
    ///put all points into the vector
    for (unsigned int i=0; i < points_L.size(); i++){
      // if (i%5 != 0){
        vector_x_l.push_back(points_L[i].x() - ego_point.x());
        vector_y_l.push_back(points_L[i].y() - ego_point.y());
      // }

    }
    vector_x_l_all.push_back(vector_x_l);
    vector_y_l_all.push_back(vector_y_l);
    ///get points of right_boundary    
    auto lines_for_cr_r = each_lane->lane().right_boundary().curve().segment(0).line_segment(); 
    std::vector<PointENU> points_R(lines_for_cr_r.point().begin(),lines_for_cr_r.point().end());
    for (unsigned int i=0; i < points_R.size(); i++){
      // if (i%5 != 0){
        vector_x_r.push_back(points_R[i].x() - ego_point.x());
        vector_y_r.push_back(points_R[i].y() - ego_point.y());
      // }
    }
    vector_x_r_all.push_back(vector_x_r);
    vector_y_r_all.push_back(vector_y_r);
    
    //get ID of succ. and pred. 
    for (const auto &succ_ids : each_lane->lane().successor_id()){
      if (succ_ids.has_id() == true){
        succ_ids_.push_back(char2double(succ_ids.id().data()));
      }
    }
    succ_ids_all.push_back(succ_ids_);

    for (const auto &pred_ids : each_lane->lane().predecessor_id()){
      if (pred_ids.has_id() == true){
        pred_ids_.push_back(char2double(pred_ids.id().data()));
      }
    }
    pred_ids_all.push_back(pred_ids_);

    /// get ID of neighbor
    for (const auto &lf_ids : each_lane->lane().left_neighbor_forward_lane_id()){
      if (lf_ids.has_id() == true){
        lf_ids_.push_back(char2double(lf_ids.id().data()));
      }
    }
    lf_ids_all.push_back(lf_ids_);

     
    for (const auto &lr_ids : each_lane->lane().left_neighbor_reverse_lane_id()){
      if (lr_ids.has_id() == true){
        lr_ids_.push_back(char2double(lr_ids.id().data()));
      }
    }
    lr_ids_all.push_back(lr_ids_);    
    

    for (const auto &rf_ids : each_lane->lane().right_neighbor_forward_lane_id()){
      if (rf_ids.has_id() == true){
        rf_ids_.push_back(char2double(rf_ids.id().data()));
      }
    }
    rf_ids_all.push_back(rf_ids_);

    
    for (const auto &rr_ids : each_lane->lane().right_neighbor_reverse_lane_id()){
      if (rr_ids.has_id() == true){
        rr_ids_.push_back(char2double(rr_ids.id().data()));
      }
    }
    rr_ids_all.push_back(rr_ids_);
    

    ///get speed_limit
    speed_li_.push_back(each_lane->lane().speed_limit());

    //
    lines_for_cr_l.clear_point();
    lines_for_cr_r.clear_point();
    std::vector<PointENU>().swap(points_L);
    std::vector<PointENU>().swap(points_R);
    std::vector<double>().swap(vector_x_l);
    std::vector<double>().swap(vector_y_l);
    std::vector<double>().swap(vector_x_r);
    std::vector<double>().swap(vector_y_r);
    std::vector<double>().swap(succ_ids_);
    std::vector<double>().swap(pred_ids_);
    std::vector<double>().swap(lf_ids_);
    std::vector<double>().swap(lr_ids_);
    std::vector<double>().swap(rf_ids_);
    std::vector<double>().swap(rr_ids_);

  }

  PyObject* laneDict = lane_parser(lane_ids_, vector_x_l_all, vector_y_l_all, vector_x_r_all,
    vector_y_r_all, succ_ids_all, pred_ids_all, lf_ids_all, lr_ids_all,
    rf_ids_all, rr_ids_all, speed_li_);
    
  return laneDict;
}

PyObject* extra_info(ADCTrajectory* trajectory_cr, const apollo::common::PointENU ego_point){

  unsigned int trajectory_point_num = trajectory_cr->trajectory_point_size();

  PyObject *state_list = PyList_New(0);  

  double last_reti = trajectory_cr->trajectory_point(0).relative_time();//Jiaying

  for(unsigned int i = 0; i < trajectory_point_num; i++){

    auto x_ = trajectory_cr->trajectory_point(i).path_point().x() - ego_point.x();
    auto y_ = trajectory_cr->trajectory_point(i).path_point().y() - ego_point.y();
    auto theta_ = trajectory_cr->trajectory_point(i).path_point().theta();
    auto v_ = trajectory_cr->trajectory_point(i).v();
    auto a_ = trajectory_cr->trajectory_point(i).a();
    auto relative_time_ = trajectory_cr->trajectory_point(i).relative_time(); //in seconds, time of this state - timestamp in header
    auto kappa_ = trajectory_cr->trajectory_point(i).path_point().kappa();

    if (i != 0){
      auto delta_t = relative_time_ - trajectory_cr->trajectory_point(i-1).relative_time();

      if (delta_t > 0.05) { //Jiaying
        
        if (cp == 0){
          cp = i;
        }
      }
      else {cp = 0;} // Jiaying

      if (relative_time_-last_reti > 0.2){//Jiaying
        double ratio = (last_reti+0.2-trajectory_cr->trajectory_point(i-1).relative_time())/delta_t;
        auto x_interp = (trajectory_cr->trajectory_point(i-1).path_point().x() - ego_point.x())*(1.0-ratio) + x_*ratio;
        auto y_interp = (trajectory_cr->trajectory_point(i-1).path_point().y() - ego_point.y())*(1.0-ratio) + y_*ratio;
        
        auto diff = theta_-trajectory_cr->trajectory_point(i-1).path_point().theta();//Jiaying_ratio
        if (diff>3.14159){
          diff = diff - 2*3.14159;
        }
        else if (diff<-3.14159){
          diff = diff + 2*3.14159;
        }
        diff = diff - (int)(diff/(2*3.1415926))*2*3.1415926;
        auto theta_interp = trajectory_cr->trajectory_point(i-1).path_point().theta() + diff*ratio;
        auto v_interp = (trajectory_cr->trajectory_point(i-1).v())*(1.0-ratio) + v_*ratio;
        auto a_interp = (trajectory_cr->trajectory_point(i-1).a())*(1.0-ratio) + a_*ratio;
        auto relative_time_interp = last_reti+0.2;
        auto kappa_interp = (trajectory_cr->trajectory_point(i-1).path_point().kappa())*(1.0-ratio) + kappa_*ratio;
        last_reti = last_reti + 0.2;

        PyObject *add_state_dict = PyDict_New();

        PyDict_SetItem(add_state_dict, PyUnicode_FromString("x"),
          PyFloat_FromDouble(x_interp));
        PyDict_SetItem(add_state_dict, PyUnicode_FromString("y"),
          PyFloat_FromDouble(y_interp));
        PyDict_SetItem(add_state_dict, PyUnicode_FromString("theta"),
          PyFloat_FromDouble(theta_interp));
        PyDict_SetItem(add_state_dict, PyUnicode_FromString("kappa"),
          PyFloat_FromDouble(kappa_interp));
        PyDict_SetItem(add_state_dict, PyUnicode_FromString("v"),
          PyFloat_FromDouble(v_interp));
        PyDict_SetItem(add_state_dict, PyUnicode_FromString("a"),
          PyFloat_FromDouble(a_interp));
        PyDict_SetItem(add_state_dict, PyUnicode_FromString("relative_time"),
          PyFloat_FromDouble(relative_time_interp));
        PyList_Append(state_list, add_state_dict);
      }
      else { // Jiaying
        continue;
      }
    }
    else{ //Jiaying
    PyObject *each_state_dict = PyDict_New();

    PyDict_SetItem(each_state_dict, PyUnicode_FromString("x"),
      PyFloat_FromDouble(x_));
    PyDict_SetItem(each_state_dict, PyUnicode_FromString("y"),
      PyFloat_FromDouble(y_));
    PyDict_SetItem(each_state_dict, PyUnicode_FromString("theta"),
      PyFloat_FromDouble(theta_));
    PyDict_SetItem(each_state_dict, PyUnicode_FromString("kappa"),
      PyFloat_FromDouble(kappa_));
    PyDict_SetItem(each_state_dict, PyUnicode_FromString("v"),
      PyFloat_FromDouble(v_));
    PyDict_SetItem(each_state_dict, PyUnicode_FromString("a"),
      PyFloat_FromDouble(a_));
    PyDict_SetItem(each_state_dict, PyUnicode_FromString("relative_time"),
      PyFloat_FromDouble(relative_time_));
    PyList_Append(state_list, each_state_dict);
    }
    }

  return state_list;
}


// This function is the main interface between commonroad and planning.cc. We put two pyobject
// including obstacle dict and lane dict into the function, pass them to the commonroad.py and return
// a pyobject trajectory (solution).  
ADCTrajectory connect_python(PyObject* obstacle_dict, PyObject* lane_dict,
  PyObject* state_list,  const apollo::common::PointENU ego_point){
  
  static int special;
  static PyObject *pModule;
  static PyObject *pFunc;
  
  ADCTrajectory unvalid;

  if (special == 0)
  {
    ++special;
    Py_Initialize();
    PyRun_SimpleString("import sys; sys.path.append('/apollo/modules/planning/planner/commonroad');");
    pModule = PyImport_ImportModule("commonroad_connect");
    if (pModule == NULL) {
      PyErr_Print();
      return unvalid;
    }
    pFunc = PyObject_GetAttrString(pModule, "commonroad");
    atexit(Py_Finalize);
  }
  
  PyObject* result = PyObject_CallFunctionObjArgs(pFunc, obstacle_dict,
    lane_dict, state_list, NULL);

  if (result == NULL){
    PyErr_Print();
    return unvalid;
  }

  ADCTrajectory traj = parse_trajectory(result, ego_point);

  // const double start_timestamp = Clock::NowInSeconds();
  // PublishPlanningPb(&traj, start_timestamp);
  // publish our commonroad trajectory to our own topic, which is subscribed only by dreamview.
  // AdapterManager::PublishCR(traj);
  
  //Py_DECREF(obstacle_dict);
  //Py_DECREF(lane_dict);
  //Py_DECREF(pFunc);
  //Py_DECREF(pModule);
  Py_DECREF(result);

  //PyGILState_Release(state);

  return traj;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

#define CHECK_ADAPTER(NAME)                                              \
  if (AdapterManager::Get##NAME() == nullptr) {                          \
    AERROR << #NAME << " is not registered";                             \
    return Status(ErrorCode::PLANNING_ERROR, #NAME " is not registerd"); \
  }

#define CHECK_ADAPTER_IF(CONDITION, NAME) \
  if (CONDITION) CHECK_ADAPTER(NAME)

void Planning::RegisterPlanners() {
  planner_factory_.Register(
      PlanningConfig::RTK, []() -> Planner* { return new RTKReplayPlanner(); });
  planner_factory_.Register(PlanningConfig::EM,
                            []() -> Planner* { return new EMPlanner(); });
  planner_factory_.Register(PlanningConfig::LATTICE,
                            []() -> Planner* { return new LatticePlanner(); });
  planner_factory_.Register(PlanningConfig::NAVI,
                            []() -> Planner* { return new NaviPlanner(); });
}

Status Planning::InitFrame(const uint32_t sequence_num,
                           const TrajectoryPoint& planning_start_point,
                           const double start_time,
                           const VehicleState& vehicle_state) {
  frame_.reset(new Frame(sequence_num, planning_start_point, start_time,
                         vehicle_state, reference_line_provider_.get()));
  auto status = frame_->Init();
  if (!status.ok()) {
    AERROR << "failed to init frame:" << status.ToString();
    return status;
  }
  return Status::OK();
}

void Planning::ResetPullOver(const routing::RoutingResponse& response) {
  auto* pull_over =
      util::GetPlanningStatus()->mutable_planning_state()->mutable_pull_over();
  if (!last_routing_.has_header()) {
    last_routing_ = response;
    pull_over->Clear();
    return;
  }
  if (!pull_over->in_pull_over()) {
    return;
  }
  if (hdmap::PncMap::IsNewRouting(last_routing_, response)) {
    pull_over->Clear();
    last_routing_ = response;
    AINFO << "Cleared Pull Over Status after received new routing";
  }
}

Status Planning::Init() {
  CHECK(apollo::common::util::GetProtoFromFile(FLAGS_planning_config_file,
                                               &config_))
      << "failed to load planning config file " << FLAGS_planning_config_file;

  CHECK(apollo::common::util::GetProtoFromFile(
      FLAGS_traffic_rule_config_filename, &traffic_rule_configs_))
      << "Failed to load traffic rule config file "
      << FLAGS_traffic_rule_config_filename;

  // initialize planning thread pool
  PlanningThreadPool::instance()->Init();

  // clear planning status
  util::GetPlanningStatus()->Clear();

  if (!AdapterManager::Initialized()) {
    AdapterManager::Init(FLAGS_planning_adapter_config_filename);
  }
  CHECK_ADAPTER(Localization);
  CHECK_ADAPTER(Chassis);
  CHECK_ADAPTER(RoutingResponse);
  CHECK_ADAPTER(RoutingRequest);
  CHECK_ADAPTER_IF(FLAGS_use_navigation_mode, RelativeMap);
  CHECK_ADAPTER_IF(FLAGS_use_navigation_mode && FLAGS_enable_prediction,
                   PerceptionObstacles);
  CHECK_ADAPTER_IF(FLAGS_enable_prediction, Prediction);
  CHECK_ADAPTER(TrafficLightDetection);

  if (!FLAGS_use_navigation_mode) {
    hdmap_ = HDMapUtil::BaseMapPtr();
    CHECK(hdmap_) << "Failed to load map";
    // Prefer "std::make_unique" to direct use of "new".
    // Reference "https://herbsutter.com/gotw/_102/" for details.
    reference_line_provider_ = std::make_unique<ReferenceLineProvider>(hdmap_);
  }

  RegisterPlanners();
  planner_ = planner_factory_.CreateObject(config_.planner_type());
  if (!planner_) {
    return Status(
        ErrorCode::PLANNING_ERROR,
        "planning is not initialized with config : " + config_.DebugString());
  }

  return planner_->Init(config_);
}

bool Planning::IsVehicleStateValid(const VehicleState& vehicle_state) {
  if (std::isnan(vehicle_state.x()) || std::isnan(vehicle_state.y()) ||
      std::isnan(vehicle_state.z()) || std::isnan(vehicle_state.heading()) ||
      std::isnan(vehicle_state.kappa()) ||
      std::isnan(vehicle_state.linear_velocity()) ||
      std::isnan(vehicle_state.linear_acceleration())) {
    return false;
  }
  return true;
}

Status Planning::Start() {
  timer_ = AdapterManager::CreateTimer(
      ros::Duration(1.0 / FLAGS_planning_loop_rate), &Planning::OnTimer, this);
  // The "reference_line_provider_" may not be created yet in navigation mode.
  // It is necessary to check its existence.
  if (reference_line_provider_) {
    reference_line_provider_->Start();
  }
  start_time_ = Clock::NowInSeconds();
  AINFO << "Planning started";
  return Status::OK();
}

void Planning::OnTimer(const ros::TimerEvent&) {
  RunOnce();

  if (FLAGS_planning_test_mode && FLAGS_test_duration > 0.0 &&
      Clock::NowInSeconds() - start_time_ > FLAGS_test_duration) {
    ros::shutdown();
  }
}

void Planning::PublishPlanningPb(ADCTrajectory* trajectory_pb,
                                 double timestamp) {
  trajectory_pb->mutable_header()->set_timestamp_sec(timestamp);
  // TODO(all): integrate reverse gear
  trajectory_pb->set_gear(canbus::Chassis::GEAR_DRIVE);
  if (AdapterManager::GetRoutingResponse() &&
      !AdapterManager::GetRoutingResponse()->Empty()) {
    trajectory_pb->mutable_routing_header()->CopyFrom(
        AdapterManager::GetRoutingResponse()->GetLatestObserved().header());
  }

  if (FLAGS_use_navigation_mode &&
      trajectory_pb->trajectory_point_size() == 0) {
    SetFallbackCruiseTrajectory(trajectory_pb);
  }

  // NOTICE:
  // Since we are using the time at each cycle beginning as timestamp, the
  // relative time of each trajectory point should be modified so that we can
  // use the current timestamp in header.

  // auto* trajectory_points = trajectory_pb.mutable_trajectory_point();
  if (!FLAGS_planning_test_mode) {
    const double dt = timestamp - Clock::NowInSeconds(); 
    for (auto& p : *trajectory_pb->mutable_trajectory_point()) {
      p.set_relative_time(p.relative_time() + dt);
    }
  }
  Publish(trajectory_pb);
}

void Planning::RunOnce() {
  // snapshot all coming data
  AdapterManager::Observe();

  const double start_timestamp = Clock::NowInSeconds();

  ADCTrajectory not_ready_pb;
  auto* not_ready = not_ready_pb.mutable_decision()
                        ->mutable_main_decision()
                        ->mutable_not_ready();
  if (AdapterManager::GetLocalization()->Empty()) {
    not_ready->set_reason("localization not ready");
  } else if (AdapterManager::GetChassis()->Empty()) {
    not_ready->set_reason("chassis not ready");
  } else if (!FLAGS_use_navigation_mode &&
             AdapterManager::GetRoutingResponse()->Empty()) {
    not_ready->set_reason("routing not ready");
  } else if (HDMapUtil::BaseMapPtr() == nullptr) {
    not_ready->set_reason("map not ready");
  }
  if (not_ready->has_reason()) {
    AERROR << not_ready->reason() << "; skip the planning cycle.";
    PublishPlanningPb(&not_ready_pb, start_timestamp);
    return;
  }

  if (FLAGS_use_navigation_mode) {
    // recreate reference line provider in every cycle
    hdmap_ = HDMapUtil::BaseMapPtr();
    // Prefer "std::make_unique" to direct use of "new".
    // Reference "https://herbsutter.com/gotw/_102/" for details.
    reference_line_provider_ = std::make_unique<ReferenceLineProvider>(hdmap_);
  }

  // localization
  const auto& localization =
      AdapterManager::GetLocalization()->GetLatestObserved();
  ADEBUG << "Get localization:" << localization.DebugString();

  // chassis
  const auto& chassis = AdapterManager::GetChassis()->GetLatestObserved();
  ADEBUG << "Get chassis:" << chassis.DebugString();

  Status status =
      VehicleStateProvider::instance()->Update(localization, chassis);

  if (FLAGS_use_navigation_mode) {
    const auto& vehicle_state_abs =
        VehicleStateProvider::instance()->vehicle_state();

    if (IsVehicleStateValid(last_vehicle_state_abs_pos_)) {
      auto x_diff = vehicle_state_abs.x() - last_vehicle_state_abs_pos_.x();
      auto y_diff = vehicle_state_abs.y() - last_vehicle_state_abs_pos_.y();
      auto theta_diff = vehicle_state_abs.heading()
          - last_vehicle_state_abs_pos_.heading();
      TrajectoryStitcher::TransformLastPublishedTrajectory(-x_diff, -y_diff,
          -theta_diff, last_publishable_trajectory_.get());
    }
    last_vehicle_state_abs_pos_ = vehicle_state_abs;

    VehicleStateProvider::instance()->set_vehicle_config(0.0, 0.0, 0.0);
  }

  VehicleState vehicle_state =
      VehicleStateProvider::instance()->vehicle_state();

  // estimate (x, y) at current timestamp
  // This estimate is only valid if the current time and vehicle state timestamp
  // differs only a small amount (20ms). When the different is too large, the
  // estimation is invalid.
  DCHECK_GE(start_timestamp, vehicle_state.timestamp());
  if (FLAGS_estimate_current_vehicle_state &&
      start_timestamp - vehicle_state.timestamp() < 0.020) {
    auto future_xy = VehicleStateProvider::instance()->EstimateFuturePosition(
        start_timestamp - vehicle_state.timestamp());
    vehicle_state.set_x(future_xy.x());
    vehicle_state.set_y(future_xy.y());
    vehicle_state.set_timestamp(start_timestamp);
  }

  if (!status.ok() || !IsVehicleStateValid(vehicle_state)) {
    std::string msg("Update VehicleStateProvider failed");
    AERROR << msg;
    not_ready->set_reason(msg);
    status.Save(not_ready_pb.mutable_header()->mutable_status());
    PublishPlanningPb(&not_ready_pb, start_timestamp);
    return;
  }

  if (!FLAGS_use_navigation_mode &&
      !reference_line_provider_->UpdateRoutingResponse(
          AdapterManager::GetRoutingResponse()->GetLatestObserved())) {
    std::string msg("Failed to update routing in reference line provider");
    AERROR << msg;
    not_ready->set_reason(msg);
    status.Save(not_ready_pb.mutable_header()->mutable_status());
    PublishPlanningPb(&not_ready_pb, start_timestamp);
    return;
  }

  if (FLAGS_enable_prediction && AdapterManager::GetPrediction()->Empty()) {
    AWARN_EVERY(100) << "prediction is enabled but no prediction provided";
  }

  // Update reference line provider and reset pull over if necessary
  if (!FLAGS_use_navigation_mode) {
    reference_line_provider_->UpdateVehicleState(vehicle_state);
    ResetPullOver(AdapterManager::GetRoutingResponse()->GetLatestObserved());
  }

  const double planning_cycle_time = 1.0 / FLAGS_planning_loop_rate;

  bool is_replan = false;
  std::vector<TrajectoryPoint> stitching_trajectory;
  stitching_trajectory = TrajectoryStitcher::ComputeStitchingTrajectory(
      vehicle_state, start_timestamp, planning_cycle_time,
      last_publishable_trajectory_.get(), &is_replan);

  const uint32_t frame_num = AdapterManager::GetPlanning()->GetSeqNum() + 1;
  status = InitFrame(frame_num, stitching_trajectory.back(), start_timestamp,
                     vehicle_state);
  if (!frame_) {
    std::string msg("Failed to init frame");
    AERROR << msg;
    not_ready->set_reason(msg);
    status.Save(not_ready_pb.mutable_header()->mutable_status());
    PublishPlanningPb(&not_ready_pb, start_timestamp);
    return;
  }
  auto* trajectory_pb = frame_->mutable_trajectory();
  if (FLAGS_enable_record_debug) {
    frame_->RecordInputDebug(trajectory_pb->mutable_debug());
  }
  trajectory_pb->mutable_latency_stats()->set_init_frame_time_ms(
      Clock::NowInSeconds() - start_timestamp);
  if (!status.ok()) {
    AERROR << status.ToString();
    if (FLAGS_publish_estop) {
      // Because the function "Control::ProduceControlCommand()" checks the
      // "estop" signal with the following line (Line 170 in control.cc):
      // estop_ = estop_ || trajectory_.estop().is_estop();
      // we should add more information to ensure the estop being triggered.
      ADCTrajectory estop_trajectory;
      EStop* estop = estop_trajectory.mutable_estop();
      estop->set_is_estop(true);
      estop->set_reason(status.error_message());
      status.Save(estop_trajectory.mutable_header()->mutable_status());
      PublishPlanningPb(&estop_trajectory, start_timestamp);
    } else {
      trajectory_pb->mutable_decision()
          ->mutable_main_decision()
          ->mutable_not_ready()
          ->set_reason(status.ToString());
      status.Save(trajectory_pb->mutable_header()->mutable_status());
      PublishPlanningPb(trajectory_pb, start_timestamp);
    }

    auto seq_num = frame_->SequenceNum();
    FrameHistory::instance()->Add(seq_num, std::move(frame_));

    return;
  }

  for (auto& ref_line_info : frame_->reference_line_info()) {
    TrafficDecider traffic_decider;
    traffic_decider.Init(traffic_rule_configs_);
    auto traffic_status = traffic_decider.Execute(frame_.get(), &ref_line_info);
    if (!traffic_status.ok() || !ref_line_info.IsDrivable()) {
      ref_line_info.SetDrivable(false);
      AWARN << "Reference line " << ref_line_info.Lanes().Id()
            << " traffic decider failed";
      continue;
    }
  }

  status = Plan(start_timestamp, stitching_trajectory, trajectory_pb);

  const auto time_diff_ms = (Clock::NowInSeconds() - start_timestamp) * 1000;
  ADEBUG << "total planning time spend: " << time_diff_ms << " ms.";

  trajectory_pb->mutable_latency_stats()->set_total_time_ms(time_diff_ms);
  ADEBUG << "Planning latency: "
         << trajectory_pb->latency_stats().DebugString();

  auto* ref_line_task =
      trajectory_pb->mutable_latency_stats()->add_task_stats();
  ref_line_task->set_time_ms(reference_line_provider_->LastTimeDelay() *
                             1000.0);
  ref_line_task->set_name("ReferenceLineProvider");

  if (!status.ok()) {
    status.Save(trajectory_pb->mutable_header()->mutable_status());
    AERROR << "Planning failed:" << status.ToString();
    if (FLAGS_publish_estop) {
      AERROR << "Planning failed and set estop";
      // Because the function "Control::ProduceControlCommand()" checks the
      // "estop" signal with the following line (Line 170 in control.cc):
      // estop_ = estop_ || trajectory_.estop().is_estop();
      // we should add more information to ensure the estop being triggered.
      EStop* estop = trajectory_pb->mutable_estop();
      estop->set_is_estop(true);
      estop->set_reason(status.error_message());
    }
  }

  trajectory_pb->set_is_replan(is_replan);

  ///////////////////////////////////////////////////////////////////////////
  //  call CommonRoad
  
  PointENU current_pose = localization.pose().position();
  auto* obstacle = extract_obstacle(frame_->obstacles(), current_pose);
  auto* lane = extract_lane(current_pose, hdmap_);
  auto* extrainfo = extra_info(trajectory_pb, current_pose);
  *trajectory_pb = connect_python(obstacle, lane, extrainfo, current_pose); 
  PublishPlanningPb(trajectory_pb, start_timestamp);
  ADEBUG << "Planning pb:" << trajectory_pb->header().DebugString();
  
  ////////////////////////////////////////////////////////////////////////////

  auto seq_num = frame_->SequenceNum();
  FrameHistory::instance()->Add(seq_num, std::move(frame_));
}

void Planning::SetFallbackCruiseTrajectory(ADCTrajectory* cruise_trajectory) {
  CHECK_NOTNULL(cruise_trajectory);

  const double v = VehicleStateProvider::instance()->linear_velocity();
  for (double t = 0.0; t < FLAGS_navigation_fallback_cruise_time; t += 0.1) {
    const double s = t * v;

    auto* cruise_point = cruise_trajectory->add_trajectory_point();
    cruise_point->mutable_path_point()->CopyFrom(
        common::util::MakePathPoint(s, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
    cruise_point->mutable_path_point()->set_s(s);
    cruise_point->set_v(v);
    cruise_point->set_a(0.0);
    cruise_point->set_relative_time(t);
  }
}

void Planning::Stop() {
  AERROR << "Planning Stop is called";
  // PlanningThreadPool::instance()->Stop();
  if (reference_line_provider_) {
    reference_line_provider_->Stop();
  }
  last_publishable_trajectory_.reset(nullptr);
  frame_.reset(nullptr);
  planner_.reset(nullptr);
  FrameHistory::instance()->Clear();
}

void Planning::SetLastPublishableTrajectory(
    const ADCTrajectory& adc_trajectory) {
  last_publishable_trajectory_.reset(new PublishableTrajectory(adc_trajectory));
}

void Planning::ExportReferenceLineDebug(planning_internal::Debug* debug) {
  if (!FLAGS_enable_record_debug) {
    return;
  }
  for (auto& reference_line_info : frame_->reference_line_info()) {
    auto rl_debug = debug->mutable_planning_data()->add_reference_line();
    rl_debug->set_id(reference_line_info.Lanes().Id());
    rl_debug->set_length(reference_line_info.reference_line().Length());
    rl_debug->set_cost(reference_line_info.Cost());
    rl_debug->set_is_change_lane_path(reference_line_info.IsChangeLanePath());
    rl_debug->set_is_drivable(reference_line_info.IsDrivable());
    rl_debug->set_is_protected(reference_line_info.GetRightOfWayStatus() ==
                               ADCTrajectory::PROTECTED);
  }
}

Status Planning::Plan(const double current_time_stamp,
                      const std::vector<TrajectoryPoint>& stitching_trajectory,
                      ADCTrajectory* trajectory_pb) {
  auto* ptr_debug = trajectory_pb->mutable_debug();
  if (FLAGS_enable_record_debug) {
    ptr_debug->mutable_planning_data()->mutable_init_point()->CopyFrom(
        stitching_trajectory.back());
  }

  auto status = planner_->Plan(stitching_trajectory.back(), frame_.get());

  ExportReferenceLineDebug(ptr_debug);

  const auto* best_ref_info = frame_->FindDriveReferenceLineInfo();
  if (!best_ref_info) {
    std::string msg("planner failed to make a driving plan");
    AERROR << msg;
    if (last_publishable_trajectory_) {
      last_publishable_trajectory_->Clear();
    }
    return Status(ErrorCode::PLANNING_ERROR, msg);
  }
  ptr_debug->MergeFrom(best_ref_info->debug());
  trajectory_pb->mutable_latency_stats()->MergeFrom(
      best_ref_info->latency_stats());
  // set right of way status
  trajectory_pb->set_right_of_way_status(best_ref_info->GetRightOfWayStatus());
  for (const auto& id : best_ref_info->TargetLaneId()) {
    trajectory_pb->add_lane_id()->CopyFrom(id);
  }

  best_ref_info->ExportDecision(trajectory_pb->mutable_decision());

  // Add debug information.
  if (FLAGS_enable_record_debug) {
    auto* reference_line = ptr_debug->mutable_planning_data()->add_path();
    reference_line->set_name("planning_reference_line");
    const auto& reference_points =
        best_ref_info->reference_line().reference_points();
    double s = 0.0;
    double prev_x = 0.0;
    double prev_y = 0.0;
    bool empty_path = true;
    for (const auto& reference_point : reference_points) {
      auto* path_point = reference_line->add_path_point();
      path_point->set_x(reference_point.x());
      path_point->set_y(reference_point.y());
      path_point->set_theta(reference_point.heading());
      path_point->set_kappa(reference_point.kappa());
      path_point->set_dkappa(reference_point.dkappa());
      if (empty_path) {
        path_point->set_s(0.0);
        empty_path = false;
      } else {
        double dx = reference_point.x() - prev_x;
        double dy = reference_point.y() - prev_y;
        s += std::hypot(dx, dy);
        path_point->set_s(s);
      }
      prev_x = reference_point.x();
      prev_y = reference_point.y();
    }
  }

  last_publishable_trajectory_.reset(new PublishableTrajectory(
      current_time_stamp, best_ref_info->trajectory()));

  ADEBUG << "current_time_stamp: " << std::to_string(current_time_stamp);

  last_publishable_trajectory_->PrependTrajectoryPoints(
      stitching_trajectory.begin(), stitching_trajectory.end() - 1);

  for (size_t i = 0; i < last_publishable_trajectory_->NumOfPoints(); ++i) {
    if (last_publishable_trajectory_->TrajectoryPointAt(i).relative_time() >
        FLAGS_trajectory_time_high_density_period) {
      break;
    }
    ADEBUG << last_publishable_trajectory_->TrajectoryPointAt(i)
                  .ShortDebugString();
  }

  last_publishable_trajectory_->PopulateTrajectoryProtobuf(trajectory_pb);

  best_ref_info->ExportEngageAdvice(trajectory_pb->mutable_engage_advice());

  return status;
}

}  // namespace planning
}  // namespace apollo