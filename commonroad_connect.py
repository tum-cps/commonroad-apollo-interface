import os
import re
import copy
import time
import string
import numpy as np

from commonroad.scenario.scenario import Scenario
from commonroad.scenario.obstacle import DynamicObstacle, StaticObstacle, ObstacleType, ObstacleRole
from commonroad.scenario.trajectory import State, Trajectory
from commonroad.scenario.lanelet import LaneletNetwork, Lanelet
from commonroad.prediction.prediction import TrajectoryPrediction
from commonroad.planning.planning_problem import PlanningProblem, PlanningProblemSet
from commonroad.planning.goal import GoalRegion
from commonroad.common.file_writer import CommonRoadFileWriter, OverwriteExistingFile
from commonroad.common.util import Interval
from commonroad.geometry.shape import Rectangle


import commonroad_dc.collision.visualization.draw_dispatch as crd
from commonroad_dc.collision.collision_detection.pycrcc_collision_dispatch import create_collision_checker
from commonroad_dc.boundary.boundary import create_road_boundary_obstacle

import sys
sys.argv = ['pdm']
import tensorflow
from dl_matcher.trajectory_approximator import TrajectoryApproximator
from dl_matcher.trajectory_generator import state_list_from_primitives

obstacle_types = {
    0: ObstacleType.CAR, # UNKNOWN
    1: ObstacleType.CAR, # PerceptionObstacle_Type_UNKNOWN_MOVABLE
    2: ObstacleType.PARKED_VEHICLE, # PerceptionObstacle_Type_UNKNOWN_UNMOVABLE
    3: ObstacleType.PEDESTRIAN,
    4: ObstacleType.BICYCLE,
    5: ObstacleType.CAR
}
################################################
######## Main functions for Interface. #########
################################################

# # create motion primitive matcher to load the database once at initialization
# model_path = "/apollo/modules/planning/planner/commonroad/dl_matcher/data/model_mlp_15_joint_norm"
# approximator = TrajectoryApproximator(model_path=model_path)
# approximator.model.load(model_path)

# approximator.init_motion_automaton(eval_mode=True)

def commonroad(obstacle, lane, extrainfo):
    print("="*100)
    # start_time = time.time()
    scenario_ = Scenario(dt=0.2, benchmark_id="apollo")
    parse_obstacle(scenario_, obstacle)
    parse_lane(scenario_, lane)
    # scenario_ = repair_lanes(scenario_)
    des_traj = parse_extrainfo(extrainfo)
    intermediate_time = time.time()
    cr_solution, ttr = call_planner(scenario_, des_traj, None)

    ADC_Traj = parse_solution(scenario_, cr_solution, extrainfo, ttr)  

    return ADC_Traj

def parse_solution(scenario, cr_solution, extrainfo, ttr):
    adc_traj_ = []
    for i, data in enumerate(cr_solution.state_list[:ttr]):
        tmp = {'x': (data.position[0]), 
               'y': (data.position[1]), 
               'o': data.orientation,
               'v': data.velocity, 
               'a': data.acceleration,
               'relative_time': extrainfo[i]["relative_time"]
               }
        adc_traj_.append(tmp)
    for data in cr_solution.state_list[ttr:]:
        relative_time = extrainfo[0]["relative_time"] + data.time_step * scenario.dt
        tmp = {'x': (data.position[0]), 
               'y': (data.position[1]), 
               'o': data.orientation,
               'v': data.velocity, 
               'a': data.acceleration,
               'relative_time': relative_time
               }
        adc_traj_.append(tmp)
       
    return adc_traj_

def parse_obstacle(scenario, o):

    for key, value in o.items(): # id, data pairs
        # velocity vectors
        v = value['v']

        # position vectors
        p_x = value['pos_x']
        p_y = value['pos_y']

        # orientation
        theta = value['theta']
        # shape
        length = value['length']
        width = value['width']

        # type
        o_type = value['type']        

        if o_type == 2:
            _role = 'static'
        else:
            _role = 'dynamic'

        obstacle_id = int(float(key)) # conversion unnecessary, change it
        obstacle_type = obstacle_types[o_type]
        obstacle_shape = Rectangle(length=length, width=width)
        obstacle_initial_state = State(position=np.array([p_x, p_y]), orientation=theta, velocity=v, time_step=0)

        if _role == 'static':
            obstacle = StaticObstacle(obstacle_id=obstacle_id, 
                                      obstacle_type=obstacle_type, 
                                      obstacle_shape=obstacle_shape, 
                                      initial_state=obstacle_initial_state)

        else:
            obstacle = DynamicObstacle(obstacle_id=obstacle_id, 
                                       obstacle_type=obstacle_type, 
                                       obstacle_shape=obstacle_shape, 
                                       initial_state=obstacle_initial_state)

        # add obstacle to scenario
        scenario.add_objects(obstacle)

def parse_lane(scenario, lane):
    step = 10.
    for lane_id_, data in lane.items():
        lanelet_ = Lanelet(
            np.array(data['left_boundary']),
            np.array(data['central_boundary']),
            np.array(data['right_boundary']),
            lane_id_, 
            data['pred_ids'], 
            data['succ_ids'],
            data['l_ids'][0], 
            data['l_ids'][1], 
            data['r_ids'][0], 
            data['r_ids'][1], 
            data['speed_limit']
        )
    # add one lanelet to the scenario.
        scenario.add_objects(lanelet_)

def parse_extrainfo(extra):
    state_list_ = []
    time_step_ = 0
    for i, data in enumerate(extra):
        x = data["x"]
        y = data["y"]
        theta = data["theta"]
        v = data["v"]
        a = data["a"]
        kappa = data["kappa"]
        state_ = State(position=np.array([x, y]), orientation=theta, velocity=v, 
            acceleration=a, yaw_rate=kappa, time_step=time_step_)
        time_step_ += 1
        state_list_.append(state_)
    return Trajectory(0, state_list_)

def call_planner(scenario, desired_trajectory: Trajectory, approximator):


    log_scenario = False

    # define planning problem
    fail_safe_branch = 10 
    goal_state_ = State(time_step = Interval(1, 400))
    goal_region_ = GoalRegion(state_list=[goal_state_])


    if len(desired_trajectory.state_list) < fail_safe_branch:
        initial_state_ = copy.deepcopy(desired_trajectory.state_list[-1])
    else:
        initial_state_ = copy.deepcopy(desired_trajectory.state_at_time_step(fail_safe_branch))
    initial_state_.slip_angle = 0.0
    planning_problem = PlanningProblem(planning_problem_id = 1,
        initial_state = initial_state_, goal_region = goal_region_) 

    # vehicle parameters: /apollo/modules/common/data/mkz_config.pb.txt
    ego_length = 4.933
    ego_width = 2.11

    if log_scenario:
        planning_problem_set = PlanningProblemSet()
        planning_problem_set.add_planning_problem(planning_problem)
        map_name = "sunnyvale_loop" # "single_lane" # "borregas_ave"
        author = 'Apollo Team'
        affiliation = 'Technical University of Munich, Germany'
        source = 'LG simulator'
        tags = ''
        path = "/apollo/modules/planning/planner/commonroad/cr_scens"
        os.makedirs(path, exist_ok=True)
        filenames = [int(re.findall(r'\d+', f)[0]) for f in os.listdir(path)]
        if len(filenames) == 0:
            index = 1
        else:
            index = max(filenames) + 1

        scenario.benchmark_id = "scenario_{}_{}".format(map_name, index)
        filename = os.path.join(path, '{}.xml'.format(scenario.benchmark_id))

        # debug desired_trajectory
        print("Saving obstacle 999999")
        dynamic_obstacle_id = 999999
        dynamic_obstacle_type = ObstacleType.CAR
        dynamic_obstacle_initial_state = desired_trajectory.state_list[0]
        dynamic_obstacle_trajectory = Trajectory(1, desired_trajectory.state_list[1:])
        dynamic_obstacle_shape = Rectangle(width = ego_width, length = ego_length)
        dynamic_obstacle_prediction = TrajectoryPrediction(dynamic_obstacle_trajectory, dynamic_obstacle_shape)
        dynamic_obstacle = DynamicObstacle(dynamic_obstacle_id, 
                                       dynamic_obstacle_type, 
                                       dynamic_obstacle_shape, 
                                       dynamic_obstacle_initial_state, 
                                       dynamic_obstacle_prediction)
        scenario.add_objects(dynamic_obstacle)
        fw = CommonRoadFileWriter(scenario, planning_problem_set, author, 
                                  affiliation, source, tags)
        fw.write_to_file(filename, OverwriteExistingFile.ALWAYS)

    # fail-safe parameter for planner
    N_fs = 30
    t_fs = 6.

    
    # create road boundaries
    (road_boundary_obstacle, road_boundary_sg_triangles) = create_road_boundary_obstacle(scenario, method='aligned_triangulation', axis=1)

    # create collision checker for scenario
    collision_checker = create_collision_checker(scenario)
    collision_checker.add_collision_object(road_boundary_sg_triangles)

    # create reference path for curvilinear coordinate system
    reference_path = obtain_reference_path(desired_trajectory, scenario)
    if reference_path.shape[0] < 5:
        reference_path = resample_polyline(reference_path, step=2.0)
    reference_path = smooth_reference(reference_path)

    # create new instance of reactive planner
    planner: ReactivePlanner = ReactivePlanner(0.2, t_fs, N_fs, factor=1)

    planner.set_reference_path(reference_path)
    planner.set_desired_velocity(desired_velocity=0., stopping=True)

    # compute TTR for branch point of fail-safe trajectory
    ttr = compute_simplified_ttr(desired_trajectory, collision_checker, planner.coordinate_system(), scenario.dt,planner.constraints)
    print(f"ttr={ttr}/{len(desired_trajectory.state_list)}")
    
    # obtain initial state for fail-safe planner and draw it
    x_0 = desired_trajectory.state_list[ttr]

    s,d = planner.coordinate_system().convert_to_curvilinear_coords(x_0.position[0],x_0.position[1])
    x_0.yaw_rate = np.interp(s, planner.coordinate_system().ref_pos(), planner.coordinate_system().ref_curv())
    x_0.slip_angle = np.interp(s, planner.coordinate_system().ref_pos(), planner.coordinate_system().ref_curv_d())

    t1 = time.time()
    # Replace solution with your planner here
    solution = desired_trajectory
    ttr = 0
    
    print(f"Elapsed time for planning: {time.time() - t1}s")
    return solution, ttr


def plot(state_list_ref, state_list_prim, filename):
    import matplotlib.pyplot as plt
    ref_color = '#666666ff'
    model_color = '#0000ffdd'

    plt.rc('font', family='serif')
    plt.figure(figsize=(6, 2))

    plt.subplots_adjust(wspace=0.75, bottom=0.275, left=0.1, right=0.95)

    lp = len(state_list_prim)
    lr = len(state_list_ref)
    state_list_prim = state_list_prim[:lr]
    state_list_ref = state_list_ref[:lp]

    ref_xs = [state.position[0] for state in state_list_ref]
    ref_ys = [state.position[1] for state in state_list_ref]
    rl_xs = [state.position[0] for state in state_list_prim]
    rl_ys = [state.position[1] for state in state_list_prim]

    ref_vels = [state.velocity for state in state_list_ref]
    rl_vels = [state.velocity for state in state_list_prim]

    ref_heading = [state.orientation for state in state_list_ref]
    rl_heading = [state.orientation for state in state_list_prim]

    plt.subplot(1, 3, 1)
    plt.plot(ref_xs, ref_ys, label="target", color=ref_color, linestyle="-")
    plt.plot(rl_xs, rl_ys, label="rl", color=model_color, linestyle="-")
    plt.axis("equal")
    plt.title("Position")

    plt.subplot(1, 3, 2)
    plt.plot(list(range(len(ref_vels))), ref_vels, label="target", color=ref_color, linestyle="-")
    plt.plot(list(range(len(rl_vels))), rl_vels, label="rl", color=model_color, linestyle="-")
    plt.axis("equal")
    plt.title("Velocity")

    plt.subplot(1, 3, 3)
    plt.plot(list(range(len(ref_heading))), ref_heading, label="target", color=ref_color, linestyle="-")
    plt.plot(list(range(len(rl_heading))), rl_heading, label="rl", color=model_color, linestyle="-")
    plt.axis("equal")
    plt.title("Orientation")

    plt.savefig(f"{filename}.png")

#     plt.show()

def repair_lanes(scenario):
    def calc_angle(point1, point2, point3):
        vector1= np.array([point1[0] - point2[0], point1[1] - point2[1]])
        vector2 = np.array([point3[0] - point2[0], point3[1] - point2[1]])      
        theta = np.arccos((vector1[0]* vector2[0] + vector1[1]*vector2[1])/
                          (np.sqrt(vector1[0]**2 + vector1[1]**2)*
                           np.sqrt(vector2[0]**2 + vector2[1]**2)))
        return theta
    new_lanelet_dict = {}
    for lanelet_old in scenario.lanelet_network.lanelets:
        best_lanelet = None
        update_points = False
        
        if lanelet_old.lanelet_id in new_lanelet_dict:
            current_lanelet = new_lanelet_dict[lanelet_old.lanelet_id]
        else:
            current_lanelet = lanelet_old
            
        middleindex = int(np.floor(len(current_lanelet.left_vertices)/2))
        theta = calc_angle(current_lanelet.left_vertices[0], current_lanelet.left_vertices[middleindex], 
                               current_lanelet.left_vertices[-1])
        
        theta_max = theta
        if current_lanelet.successor:
            for succ_lanelet_id in current_lanelet.successor:
                if succ_lanelet_id in new_lanelet_dict:
                    succ_lanelet = new_lanelet_dict[succ_lanelet_id]
                else:
                    succ_lanelet = scenario.lanelet_network.find_lanelet_by_id(succ_lanelet_id)
                    
                middle_index = int(np.floor(len(succ_lanelet.left_vertices)/2))
                theta_succ = calc_angle(succ_lanelet.left_vertices[0], succ_lanelet.left_vertices[middle_index], 
                               succ_lanelet.left_vertices[-1])
                if theta_succ > theta_max:
                    theta_max = theta_succ
                    best_lanelet = succ_lanelet
                    

                if not update_points:
                    if not ((succ_lanelet.left_vertices[0][0] == current_lanelet.left_vertices[-1][0]) 
                    and (succ_lanelet.left_vertices[0][1] == current_lanelet.left_vertices[-1][1])):  
                        update_points = True
                 
            # Case reference for updating is not current_lanelet
            if update_points and best_lanelet is not None:
                # Add best_lanelet to new_lanelet_dict
                if best_lanelet.lanelet_id not in new_lanelet_dict:
                    new_lanelet_dict[best_lanelet.lanelet_id] = best_lanelet
                # Adapt current lanelet
                current_lanelet = Lanelet(left_vertices=np.concatenate(
                    (current_lanelet.left_vertices[0:-1],[best_lanelet.left_vertices[0]]), axis=0), 
                                          center_vertices=np.concatenate(
                    (current_lanelet.center_vertices[0:-1], [best_lanelet.center_vertices[0]]), axis=0), 
                                          right_vertices=np.concatenate(
                    (current_lanelet.right_vertices[0:-1],[best_lanelet.right_vertices[0]]), axis=0), 
                                          lanelet_id=current_lanelet.lanelet_id, 
                                      predecessor=current_lanelet.predecessor, 
                                          successor=current_lanelet.successor, 
                                      adjacent_left=current_lanelet.adj_left, 
                                      adjacent_left_same_direction=current_lanelet.adj_left_same_direction,
                                      adjacent_right=current_lanelet.adj_right, 
                                      adjacent_right_same_direction=current_lanelet.adj_right_same_direction,
                                      speed_limit=current_lanelet.speed_limit, 
                                      line_marking_left_vertices=current_lanelet.line_marking_left_vertices, 
                                      line_marking_right_vertices=current_lanelet.line_marking_right_vertices)
                # Adapt all others successors
                for succ_lanelet_id in current_lanelet.successor:
                    if not (succ_lanelet_id == best_lanelet.lanelet_id):
                        if succ_lanelet_id in new_lanelet_dict:
                            succ_lanelet = new_lanelet_dict[succ_lanelet_id]  
                        else:
                            succ_lanelet = scenario.lanelet_network.find_lanelet_by_id(succ_lanelet_id)
                            
                            
                        new_lanelet_dict[succ_lanelet_id] = Lanelet(left_vertices=np.concatenate(
                            ([best_lanelet.left_vertices[0]], succ_lanelet.left_vertices[1:]), axis=0), 
                                                                    center_vertices=np.concatenate(
                            ([best_lanelet.center_vertices[0]], succ_lanelet.center_vertices[1:],), axis=0), 
                                      right_vertices=np.concatenate(
                            ([best_lanelet.right_vertices[0]], succ_lanelet.right_vertices[1:]), axis=0), 
                                      lanelet_id=succ_lanelet.lanelet_id, 
                                  predecessor=succ_lanelet.predecessor, 
                                      successor=succ_lanelet.successor, 
                                  adjacent_left=succ_lanelet.adj_left, 
                                  adjacent_left_same_direction=succ_lanelet.adj_left_same_direction,
                                  adjacent_right=succ_lanelet.adj_right, 
                                  adjacent_right_same_direction=succ_lanelet.adj_right_same_direction,
                                  speed_limit=succ_lanelet.speed_limit, 
                                  line_marking_left_vertices=succ_lanelet.line_marking_left_vertices, 
                                  line_marking_right_vertices=succ_lanelet.line_marking_right_vertices)
                
                            
            elif update_points:
                # update first vertex of all successors:
                for succ_lanelet_id in current_lanelet.successor:
                    if succ_lanelet_id in new_lanelet_dict:
                        succ_lanelet = new_lanelet_dict[succ_lanelet_id]  
                    else:
                        succ_lanelet = scenario.lanelet_network.find_lanelet_by_id(succ_lanelet_id)
                            
                    new_lanelet_dict[succ_lanelet_id] = Lanelet(left_vertices=np.concatenate(
                        ([current_lanelet.left_vertices[-1]], succ_lanelet.left_vertices[1:]), axis=0),
                                                                center_vertices=np.concatenate(
                        ([current_lanelet.center_vertices[-1]], succ_lanelet.center_vertices[1:]), axis=0),
                                                                right_vertices=np.concatenate(
                        ([current_lanelet.right_vertices[-1]], succ_lanelet.right_vertices[1:]), axis=0), 
                                                                lanelet_id=succ_lanelet.lanelet_id, 
                                                                predecessor=succ_lanelet.predecessor, 
                                                                successor=succ_lanelet.successor, 
                                                                adjacent_left=succ_lanelet.adj_left, 
                                                                adjacent_left_same_direction=succ_lanelet.adj_left_same_direction,
                                                                adjacent_right=succ_lanelet.adj_right, 
                                                                adjacent_right_same_direction=succ_lanelet.adj_right_same_direction,
                                                                speed_limit=succ_lanelet.speed_limit, 
                                                                line_marking_left_vertices=succ_lanelet.line_marking_left_vertices, 
                                                                line_marking_right_vertices=succ_lanelet.line_marking_right_vertices)
               
        theta_max = theta
        update_points = False
        best_lanelet = None
        if current_lanelet.predecessor:
            for pred_lanelet_id in current_lanelet.predecessor:
                if pred_lanelet_id in new_lanelet_dict:
                    pred_lanelet = new_lanelet_dict[pred_lanelet_id]
                else:
                    pred_lanelet = scenario.lanelet_network.find_lanelet_by_id(pred_lanelet_id)
                    
                middle_index = int(np.floor(len(pred_lanelet.left_vertices)/2))
                theta_pred = calc_angle(pred_lanelet.left_vertices[0], pred_lanelet.left_vertices[middle_index], 
                               pred_lanelet.left_vertices[-1])
                if theta_pred > theta_max:
                    theta_max = theta_pred
                    best_lanelet = pred_lanelet
                if not update_points:
                    if not ((pred_lanelet.left_vertices[-1][0] == current_lanelet.left_vertices[0][0]) 
                    and (pred_lanelet.left_vertices[-1][1] == current_lanelet.left_vertices[0][1])):
                        update_points = True
                 
            # Case reference for updating is not current_lanelet
            if update_points and best_lanelet is not None:
                # Add best_lanelet to new_lanelet_dict
                if best_lanelet.lanelet_id not in new_lanelet_dict:
                    new_lanelet_dict[best_lanelet.lanelet_id] = best_lanelet
                # Adapt current lanelet
                current_lanelet = Lanelet(left_vertices=np.concatenate(
                    ([best_lanelet.left_vertices[-1]], current_lanelet.left_vertices[1:]), axis=0), 
                                          center_vertices=np.concatenate(
                    ([best_lanelet.center_vertices[-1]], current_lanelet.center_vertices[1:]), axis=0), 
                                          right_vertices=np.concatenate(
                    ([best_lanelet.right_vertices[-1]], current_lanelet.right_vertices[1:]), axis=0), 
                                          lanelet_id=current_lanelet.lanelet_id, 
                                      predecessor=current_lanelet.predecessor, 
                                          successor=current_lanelet.successor, 
                                      adjacent_left=current_lanelet.adj_left, 
                                      adjacent_left_same_direction=current_lanelet.adj_left_same_direction,
                                      adjacent_right=current_lanelet.adj_right, 
                                      adjacent_right_same_direction=current_lanelet.adj_right_same_direction,
                                      speed_limit=current_lanelet.speed_limit, 
                                      line_marking_left_vertices=current_lanelet.line_marking_left_vertices, 
                                      line_marking_right_vertices=current_lanelet.line_marking_right_vertices)
                # Adapt all others predecessors
                for pred_lanelet_id in current_lanelet.predecessor:
                    if not (pred_lanelet_id == best_lanelet.lanelet_id):
                        if pred_lanelet_id in new_lanelet_dict:
                            pred_lanelet = new_lanelet_dict[pred_lanelet_id]  
                        else:
                            pred_lanelet = scenario.lanelet_network.find_lanelet_by_id(pred_lanelet_id)
                            
                        new_lanelet_dict[pred_lanelet_id] = Lanelet(left_vertices=np.concatenate(
                            (pred_lanelet.left_vertices[0:-1], [best_lanelet.left_vertices[-1]]), axis=0), 
                                                                    center_vertices=np.concatenate(
                            (pred_lanelet.center_vertices[0:-1], [best_lanelet.center_vertices[-1]]), axis=0), 
                                      right_vertices=np.concatenate(
                            (pred_lanelet.right_vertices[0:-1], [best_lanelet.right_vertices[-1]]), axis=0), 
                                      lanelet_id=pred_lanelet.lanelet_id, 
                                  predecessor=pred_lanelet.predecessor, 
                                      successor=pred_lanelet.successor, 
                                  adjacent_left=pred_lanelet.adj_left, 
                                  adjacent_left_same_direction=pred_lanelet.adj_left_same_direction,
                                  adjacent_right=pred_lanelet.adj_right, 
                                  adjacent_right_same_direction=pred_lanelet.adj_right_same_direction,
                                  speed_limit=pred_lanelet.speed_limit, 
                                  line_marking_left_vertices=pred_lanelet.line_marking_left_vertices, 
                                  line_marking_right_vertices=pred_lanelet.line_marking_right_vertices)
                
                            
            elif update_points:
                # update first vertex of all predecessors:
                for pred_lanelet in current_lanelet.predecessor:
                    if pred_lanelet_id in new_lanelet_dict:
                            pred_lanelet = new_lanelet_dict[pred_lanelet_id]
                    else:
                        pred_lanelet = scenario.lanelet_network.find_lanelet_by_id(pred_lanelet_id)
                            
                    new_lanelet_dict[pred_lanelet_id] = Lanelet(left_vertices=np.concatenate(
                        (pred_lanelet.left_vertices[0:-1], [current_lanelet.left_vertices[0]]), axis=0),
                                                                center_vertices=np.concatenate(
                        (pred_lanelet.center_vertices[0:-1], [current_lanelet.center_vertices[0]]), axis=0),
                                                                right_vertices=np.concatenate(
                        ( pred_lanelet.right_vertices[0:-1], [current_lanelet.right_vertices[0]]), axis=0), 
                                                                lanelet_id=pred_lanelet.lanelet_id, 
                                                                predecessor=pred_lanelet.predecessor, 
                                                                successor=pred_lanelet.successor, 
                                                                adjacent_left=pred_lanelet.adj_left, 
                                                                adjacent_left_same_direction=pred_lanelet.adj_left_same_direction,
                                                                adjacent_right=pred_lanelet.adj_right, 
                                                                adjacent_right_same_direction=pred_lanelet.adj_right_same_direction,
                                                                speed_limit=pred_lanelet.speed_limit, 
                                                                line_marking_left_vertices=pred_lanelet.line_marking_left_vertices, 
                                                                line_marking_right_vertices=pred_lanelet.line_marking_right_vertices)
                
                                
        new_lanelet_dict[current_lanelet.lanelet_id] = current_lanelet    
    
    scenario.lanelet_network = LaneletNetwork.create_from_lanelet_list(list(new_lanelet_dict.values()))

    return scenario
