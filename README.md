# Installation Guide

This installation guide is to install docker, Apollo 3.0 and CommonRoad packages on Ubuntu systems. We tested with Ubuntu 16.04 and Ubuntu 18.04.



## Docker

In order to use Apollo you have first to install docker (https://docs.docker.com/install/linux/docker-ce/ubuntu/)

1. **Update the apt package index**:

``
    $ sudo apt-get update
``

2. **Install packages** to allow apt to use a repository over HTTPS:

``
    $ sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
``

3. **Add Docker’s official GPG key**:

``
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
``

4. **Verify** that you now have the key with the fingerprint **9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88**, by searching for the last 8 characters of the fingerprint.

``
    $ sudo apt-key fingerprint 0EBFCD88
``

   
5. Use the following command to **set up the stable repository**. To add the nightly or test repository, add the word nightly or test (or both) after the word stable in the commands below. Learn about nightly and test channels.

``
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
``

6. Update the **apt** package index.

``
	$ sudo apt-get update
``

**Install Docker CE**
 
``
	$ sudo apt-get install docker-ce docker-ce-cli containerd.i
``

7. **Check docker-installation**:

``
    $ sudo docker run hello-world
``

The output should start with: 'Hello from Docker! ...'

8. Posti-installation steps for Linux:

8.1 Create **docker** group.

``
    $ sudo groupadd docker
``

8.2 Add your user to the **docker** group.

``
    $ sudo usermod -aG docker $USER
``

8.3 Log out and log back in so that your group membership is re-evaluated.

If testing on a virtual machine, it may be necessary to restart the virtual machine for changes to take effect.

On a desktop Linux environment such as X Windows, log out of your session completely and then log back in.

8.4 Verify that you can run **docker** commands without **sudo**.

``
    $ docker run hello-world
``



## Apollo

1. Download **Apollo 3.0** and extract it to a local folder (https://github.com/ApolloAuto/apollo/releases).

1.5 Open a terminal and navigate to the 'apollo-3.0.0' folder:

``
     $ cd your/path/.../apollo-3.0.0/
``

**Alternatively**, you can also git clone the official repository:

``
	$ git clone https://github.com/ApolloAuto/apollo.git
``

And then checkout the v3.0.0 release:

``
	$ git checkout v3.0.0
``

To check if you download the right version, open README.md and check whether you see something like "The Apollo Team now proudly presents to you the latest [version 3.0]".


2. Start the docker release environment using the command:

``
	$ bash docker/scripts/dev_start.sh
``

3. Enter the docker release environment:

``
	$ bash docker/scripts/dev_into.sh
``

4. Build Apollo in the Container:

``
	$ bash apollo.sh build
``
	
5. Bootstrap to start ros call and Monitor module and Dreamview

``
	$ bash scripts/bootstrap.sh
``

(If you want to stop the Monitor module and Dreamview, run:

``
	$ bash scripts/bootstrap.sh stop
``

)


6. Download rosbag file in order to test your installation

``
    $ sudo python docs/demo_guide/rosbag_helper.py demo_2.5.bag
``

7. Play the rosbag that you just downloaded.

``  
    $ rosbag play demo_2.5.bag --loop
``

if you receive the message "command not found" you have to source the appropriate ros file:

 ``
    $ source /apollo/bazel-apollo/external/ros/setup.bash
``

8. Open Chrome and go to **localhost:8888** to access Apollo Dreamview.



9. You probably only see a car in the Dreamview window. Here are the steps to **start the planning module**:


* in `Tasks - Others`  turn on the `SimControl` button

* in `Module Controller - Modules` turn on the `Planning` button, the `Routing` button, the `Control` button and the `Localization` button.

(you need the routing module as well cause it provides the reference line for the planner)


10. In case you come across the 'Are your messages built error' when trying to print out messages being published to  the topics, you have to add the py_proto folder to the PYTHONPATH variable inside docker

``
export PYTHONPATH=${PYTHONPATH}:/apollo/py_proto/ 
``

(If you get error "ERROR: XXX: undeclared inclusion(s) in rule 'XXX'" while building, it means that the bazel dependencies are broken. You can run `bazel clean --expunge` to clean the broken dependencies and then build again.)


## Install CommonRoad Softwares in Apollo docker container

### Install commonroad-io in Apollo docker container

Apollo container already has miniconda2, so you only have to create a conda environment inside docker and then install commonroad-io in the conda environment. Here are the commands you need (remember you need to *dev_into apollo container* first):

   ``conda create -n commonroad_py36 python=3.6``

   ``source activate commonroad_py36``
 
   ``sudo pip2 uninstall enum34``

   ``sudo pip2 uninstall shapely``

   ``sudo rm -rf /home/tmp/ros/lib/python2.7/dist-packages/numpy``

   ``pip install commonroad-io==2019.2``

   ``pip install scipy``

Now you are finished with installation. You can test by 

   ``source activate commonroad_py36``

   ``python``

   ``import commonroad``



### Install commonroad-drivability-checker in Apollo docker container 

1. Increase the version of gcc to at least 7.0:

* Check current gcc version:

   ``gcc --version``

* Add repository to ubuntu for g++ installation and install g++:

   ``sudo add-apt-repository ppa:ubuntu-toolchain-r/test``

   ``sudo apt-get update``

   ``sudo apt-get install -y g++-7``

* Replace old gcc/g++ version with new version:

   ``cd /usr/bin``

   ``sudo rm -r gcc``

   ``sudo rm -r g++``

   ``sudo ln -sf gcc-7 gcc``

   ``sudo ln -sf g++-7 g++``

* Check new gcc version:

   ``gcc --version``

* Install libgfortran-7-dev:

   ``sudo apt install libgfortran-7-dev``


2. Upgrade eigen to at least version 3.2.7:

   (Note: Make sure you are in the apollo root folder `cd /apollo`)

* Download eigen-3.2.10:

   ``wget http://bitbucket.org/eigen/eigen/get/3.2.10.tar.gz``

* Unpack tar file and delete tar file:

   ``tar xzvf 3.2.10.tar.gz``

   ``rm 3.2.10.tar.gz``

* Upgrade eigen:

   ``cd eigen-eigen-b9cd8366d4e8``

   ``sudo rm -rf /usr/include/eigen3/Eigen/``

   ``sudo cp -r Eigen /usr/include/eigen3/.``

3. Upgrade cmake to at least cmake 3.2:

   (Note: Make sure you are in the apollo root folder `cd /apollo`)

* Download cmake-3.2:

   ``wget http://www.cmake.org/files/v3.2/cmake-3.2.0.tar.gz``

* Unpack tar file and delete tar file:

   ``tar xzvf cmake-3.2.0.tar.gz``

   ``rm cmake-3.2.0.tar.gz``

* Install cmake-3.2:

   ``cd cmake-3.2.0``

   ``./configure``

   ``make -j4``

   ``sudo make install``



4. Install and build commonroad-drivability-checker

```
source activate commonroad_py36

git clone git@gitlab.lrz.de:tum-cps/commonroad-drivability-checker.git

cd commonroad-drivability-checker
```

add following lines in CMakeLists.txt (after cmake_minimum_required(VERSION 3.0)) :
  set(BOOST_ROOT /usr/local/boost1.66)
  set(Boost_NO_SYSTEM_PATHS on)
  find_package(Boost)
  include_directories(${Boost_INCLUDE_DIRS})

```
bash build.sh -e $CONDA_PREFIX -v 3.6 --serializer --no-root -j 10 -c ${CONDA_PREFIX}/lib/python3.6/site-packages
cp ./commonroad_dc/pycrcc.cpython-36m-x86_64-linux-gnu.so ./pycrcc.cpython-36m-x86_64-linux-gnu.so
cp ./commonroad_dc/libcrcc.a ./libcrcc.a
cd ..

echo "$(pwd)/commonroad-drivability-checker" >> "${CONDA_PREFIX}/lib/python3.6/site-packages/name.pth"

pip install Polygon3
```

Test if installation works by running the jupyter notebooks in the tutorials folder.



## Downgrade gcc to 4.8:

   `cd /usr/bin`

   `sudo rm -r gcc`

   `sudo rm -r g++`

   `sudo ln -sf gcc-4.8 gcc`

   `sudo ln -sf g++-4.8 g++`

   `gcc --version`

## Activate conda environment and build Apollo again: 

   (Note: Make sure you are in the apollo root folder: `cd /apollo`)

   ``source activate commonroad_py36``

   ``bash apollo.sh build``


## Setup CommonRoad Compatible Planner

1. Replace the ``planning.cc`` and ``BUILD`` files in the folder ``/apollo/modules/planning/`` with the ones we provided.

2. Replace the ``WORKSPACE.in`` in the folder ``/apollo`` with the one we provided.

3. In ``apollo/WORKSPACE.in`` change the path of ``python_linux`` with the path of your ``commonroad_py36`` conda environment.

(You can check the path with command ``conda info -e``.)

4. ``mkdir /apollo/modules/planning/planner/commonroad``

5. Put file ``commonroad_connect.py`` in the folder ``/apollo/modules/planning/planner/commonroad``.

6. Copy the folder of our fail-safe planner in ``/apollo/modules/planning/planner/commonroad``.

7. Add following path to `/path/to/your/.conda/envs/commonroad_py36/lib/python3.6/site-packages/name.pth` with content:

    ``/apollo/modules/planning/planner/commonroad/folder_name_of_your_planner``

8. Activate conda environment and build Apollo again

``source activate commonroad_py36``

``bash apollo.sh build``



## Store everything you just installed in Apollo docker container permanently

1. Open another terminal, go to the directory that Apollo is installed and check which containers run at this moment:

   `docker container ls`

The result should give the five containers id that Apollo uses.

2. Commonroad is now installed to the container which runs the image : apolloauto/apollo:dev-x86_64-20180702_1140. We want to commit on this container the changes that we have done and save everything in a new image:

    `docker commit [containerId] [apolloauto/apollo:new_image_name]` 

    (e.g. docker commit 6f37f622b22b apolloauto/apollo:dev-x86_64-20180702_1140_with_cr)

3. In order to use the image we just created and not pull the image from Apollo's repository, the following modifications are needed: 

   *  line 23: change image name variable `VERSION_X86_64` to new image name you just assigned

   *  line 210: comment out `command docker pull $IMG`

4. Run the new script in order to start all the containers that are needed for Apollo.

[return](https://gitlab.lrz.de/ga92tuj/cr_apollo/wikis/01.-Installation-Guide#installation-guide)